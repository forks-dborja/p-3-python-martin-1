# Ejercicio 4
import unittest
from mysound import Sound
import math


class TestSound(unittest.TestCase):
    # Crearé un test que me compruebe
    # que la funcion sin() funciona correctamente.

    # Doy un valor de tiempo y me devuelve
    # el valor correcto de la onda senoidal en ese punto.

    # Si pongo t = 0 en sound.buffer[]
    # debere poner 44100 * 0 = 0
    # Si pongo t = 0.1 en sound.buffer[]
    # debere poner 44100 * 0.1 = 4410
    # Si pongo t = 0.1 en sound.buffer[]
    # debere poner 44100 * 0.2 = 8820
    def test_sin_wave(self):
        freq = 440
        amp = 10000

        duration = 1
        s = Sound(duration)
        s.sin(freq, amp)

        expected = amp * math.sin(2 * math.pi * freq * 0)
        self.assertEqual(expected, s.buffer[0])

        expected = amp * math.sin(2 * math.pi * freq * 0.1)
        (self.assertAlmostEqual(expected, s.buffer[4410], delta=1))

        expected = amp * math.sin(2 * math.pi * freq * 0.2)
        self.assertAlmostEqual(expected, s.buffer[8820], delta=1)
